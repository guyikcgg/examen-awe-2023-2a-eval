/* Scrum Test */
const submitBtn = document.querySelector('#scrum input[type=button]');
const form = document.querySelector('#scrum form');

submitBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const data = new FormData(form);
    const json = Object.fromEntries(data.entries());
    console.log(json);
    dl.href = "data:text/plain," + encodeURIComponent(JSON.stringify(json));
    dl.click();
});

afirmaciones = [
    "El objetivo último de Scrum es entregar más valor a los usuarios y organizar cómo se entrega ese valor a lo largo del tiempo.",
    "Comúnmente, Scrum se define como una metodología ágil.",
    "Scrum es un marco de trabajo que establece unas reglas, como roles, artefactos y eventos, que nos ayudan a mantener la transparencia durante todo el ciclo de desarrollo de nuestro producto.",
    "El objetivo de Scrum es ayudar al equipo a completar todas las tareas del Product Backlog en el período definido por el Sprint.",
    "Todos los elementos del Product Backlog deben ser historias de usuario descritas con la notación \"COMO... QUIERO... PARA...\".",
    "Scrum comienza con la definición exhaustiva de los requisitos software en el Product Backlog, que sólo se modificará para marcar los requisitos como implementados.",
    "Conocemos por historia de usuario a una explicación general e informal de una función de software escrita desde la perspectiva del usuario final. Su objetivo es explicar cómo una función del software aportará valor al cliente.",
    "Las historias de usuario especifican exactamente cómo deberá implementarse el software para generar el máximo valor al cliente.",
    "El Product Owner se encarga de ordenar el Product Backlog atendiendo a las prioridades de todas las partes implicadas (stakeholders).",
    "El Product Backlog puede definirse como la lista ordenada de trabajo que contiene descripciones breves de todas las características y correcciones deseadas para el producto.",
    "Scrum Poker es una modalidad del conocido juego de cartas, en la que el equipo se reúne con el cliente a fin de comprender mejor sus prioridades.",
    "El resultado final del Planning Meeting es la definición del Sprint Backlog y del Sprint Goal.",
    "Una historia de usuario debe ser atómica, es decir, no debe subdividirse en tareas.",
    "El Scrum Master es el encargado de que Scrum funcione y de ayudar a eliminar impedimentos.",
    "El Product Owner es el encargado de maximizar el valor y de gestionar el Product Backlog.",
    "En nuestro ciclo de desarrollo, Git nos ayuda con CD, mientras que Gitlab Pages nos ayuda con CI.",
    "CI/CD permite compilar los cambios incrementales de código realizados por los desarrolladores, integrándolos en aplicaciones software que los usuarios pueden testear.",
    "En el Daily Meeting, los desarrolladores inspeccionan el progreso hacia el Sprint Goal, adaptando el Sprint Backlog en caso de que sea necesario.",
    "Cada <code>git commit</code> debe suponer un incremento de valor en el producto final.",
    "En el Sprint Review Meeting, el equipo inspecciona el estado del incremento junto al cliente, a fin de adaptar el Product Backlog atendiendo a los comentarios de todas las partes implicadas."
];

preguntado = [];

n_afirmaciones = 0;
while(n_afirmaciones < 10) {
    for (i=0; i<afirmaciones.length && n_afirmaciones < 10; i++) {
        afirmacion = afirmaciones[i];
        id = "afirmacion-" + i;
        
        if(Math.round(rand()) || i in preguntado) continue;

        n_afirmaciones++;
        preguntado.push(i);

        n = document.createElement("option");
        n.setAttribute("value", "");
        n.appendChild(document.createTextNode(""));
        v = document.createElement("option");
        v.setAttribute("value", "V");
        v.appendChild(document.createTextNode("Verdadero"));
        f = document.createElement("option");
        f.setAttribute("value", "F");
        f.appendChild(document.createTextNode("Falso"));

        vf = document.createElement("select");
        vf.setAttribute("name", id);
        vf.setAttribute("id", id);
        vf.appendChild(n);
        vf.appendChild(v);
        vf.appendChild(f);
    
        bold = document.createElement("b");
        bold.appendChild(document.createTextNode(n_afirmaciones + ". "));

        label = document.createElement("label");
        label.appendChild(bold);
        label.appendChild(document.createTextNode(afirmacion));
        label.setAttribute("for", id);

        form.insertBefore(label, submitBtn);
        form.insertBefore(vf, submitBtn);
    }
}

/* PBI Selection */
pbi1_name = "#pbi-" + Math.round(rand() + 1);
pbi2_name = "#pbi-" + Math.round(rand() + 3);

pbi1 = document.querySelector(pbi1_name);
pbi1.setAttribute("class", "enabled");
pbi2 = document.querySelector(pbi2_name);
pbi2.setAttribute("class", "enabled");

txt_tareas = "Test, " + document.querySelector(pbi1_name + ' .pbi-name').innerHTML + ", " + document.querySelector(pbi2_name + ' .pbi-name').innerHTML + '.';


const tareas = document.querySelector('#tareas');
tareas.innerHTML = txt_tareas;
